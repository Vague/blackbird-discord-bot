const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json'); // secrets
const schedule = require('node-schedule');
const messages = require('./messages.json');


// For the counter responder
var lastmsgsent = Math.floor(Date.now() / 1000)
var reg = new RegExp('^[0-9]+$');
function between(min, max) {  
  return Math.floor(
    Math.random() * (max - min) + min
  )
}

// regular pinging
function animemessage(chat){
	msg = messages[Math.floor(Math.random()*messages.length)];
	msg =  config.pingrole + " " + msg ;
	//console.log(msg);
	client.channels.cache.get(chat).send(msg);
}

// anime ping 1 , mwf 6:00 pm
schedule.scheduleJob('0 18 * * 1,3,5', function(){
	console.log('sending anime ping');
	animemessage(config.chattheater);
});

// anime ping 2 , mwf 6:20 pm 
schedule.scheduleJob('20 18 * * 1,3,5', function(){
	console.log('sending anime ping');
	animemessage(config.chattheater);
});

// movie ping 1 , sat 6:00 pm
schedule.scheduleJob('0 18 * * 6', function(){
	console.log('sending movie ping');
	animemessage(config.chattheater);
});

// movie ping 2 , sat 6:20 pm
schedule.scheduleJob('20 18 * * 6', function(){
	console.log('sending movie ping');
	animemessage(config.chattheater);
});

client.once('ready', () => {
	console.log('Ready!');
});

client.on('message', message => {
	console.log(message.content);
	// counting chat stuff
	// the counting chat has only messages like '1' '2' '3' '4' with the goal being to count to 10,000. each participant can only message one number at a time. 
	// on every message sent to the chat, blackbird has a varying chance to respond with the increment of the last message. 
	// the chance of reponse is 1 out of a number that varies. the number is in the range of 0 to (3600 - seconds since last counting message sent by blackbird). the range of the number is between 0 and 3600 depending on time. after an hour the range contains only 0, which causes blackbird to send the increment of the next message with no random chance component. 
	if ( reg.test(message.content) ){
		console.log("msg is number")
		try {
			var parsed = parseInt(message.content,10)
			if (message.author.username === 'blackbird'){
				console.log('ignore self')
			} else {
				if (message.channel.id === config.countchat){
					tnow = Math.floor(Date.now() / 1000)
					timesincelastmsg = tnow - lastmsgsent	
					if ( timesincelastmsg > 3600 ){
						timesincelastmsg = 3600
					}
					var msgrand = between(0,(3600 - timesincelastmsg ))
					console.log('timesincelastmsg ' + timesincelastmsg )
					console.log('rolling ' + msgrand)
					if (msgrand === 0){
						message.channel.send(++parsed);
						lastmsgsent = Math.floor(Date.now() / 1000)
					}
				}
			}
		}
		catch(e){
			console.log("broke")
		}
	}
	if (message.content === '!squawk') {
		message.channel.send('it is me, comrad!');
	}
});

client.login(config.token);
